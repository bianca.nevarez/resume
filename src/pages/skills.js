import React from "react";
import  {Container} from 'react-bootstrap';
import "./skills.scss";
import BasicLayout from "../Layouts/BasicLayout";
import ListSkills from "../components/ListSkills";

import { habilidades, software, habilidadesSkillsColor, softwareSkillsColor} from '../utils/skills';

export default function Index(){

  
  const skills = habilidades.map( (item, index) => {
      return (
        <div>
          <p> { item.label }</p>
          <ListSkills skills = {item.bar} colors = { habilidadesSkillsColor }/>
        </div>
      );
  })

  const softSkills = software.map( (item, index) => {
    return (
      <div>
        <p> { item.label }</p>
        <ListSkills skills = {item.bar} colors = { softwareSkillsColor }/>
      </div>
    );
})

  return (
    <BasicLayout menuColor = "#000">
      <Container>
        <div className="skills__block">
          <h2>Habilidades </h2>
          { skills }
        </div>
        <div className="skills__block">
          <h2>Software</h2>
          {softSkills}
        </div>
        <div className="skills__block"/>
      </Container>
    </BasicLayout>
  );
}
  

