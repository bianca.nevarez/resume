import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";

import BasicLayout from "../Layouts/BasicLayout";
import './projects.scss';
import projects from "../utils/projects";
import experiencia from "../utils/experiencia";

export default function Projects (props){

  return (
    <BasicLayout menuColor = "#000">
      <Container className="projects">
        <h1> Trayectoria academica </h1>
        
        <Row>

          { 
            projects.map(
              ( project, index ) => (
                <Col key = {index} xs={12} sm= {4} className = "project" >
                  <Card>
                    <div 
                      className = "image" 
                      style = {{ 
                        backgroundImage : `url("${project.image}")`
                      }} 
                    />
                    <Card.Body>
                      <Card.Title>
                        {project.title}
                      </Card.Title>
                      <Card.Text>
                        {project.description}
                      </Card.Text>
                      <a href={project.url} target= "_blank" rel="noreferrer" > 
                        <Button variant = "primary"> ver web</Button>
                      </a>
                    </Card.Body>
                  </Card>
                </Col>
              )
            )
          }
          
        </Row>
      </Container>
      <Container className="projects">
        <h1> Experiencia laboral </h1>
        
        <Row>

          { 
            experiencia.map(
              ( work, index ) => (
                <Col key = {index} xs={12} sm= {4} className = "project" >
                  <Card>
                    <div 
                      className = "image" 
                      style = {{ 
                        backgroundImage : `url("${work.image}")`
                      }} 
                    />
                    <Card.Body>
                      <Card.Title>
                        {work.title}
                      </Card.Title>
                      <Card.Text>
                        {work.description}
                      </Card.Text>
                      <a href={work.url} target= "_blank" rel="noreferrer" > 
                        <Button variant = "primary"> ver web</Button>
                      </a>
                    </Card.Body>
                  </Card>
                </Col>
              )
            )
          }
          
        </Row>
      </Container>
    </BasicLayout>
  );
}
