export const habilidades = [
    {   
        label : "Bilingüe",
        bar: [{type: "Dominio", level: 80 }]
    },
    {   
        label : "Trabajo bajo presión",
        bar: [{type: "Dominio", level: 80 }]
    },
    {   
        label : "Atención al cliente",
        bar: [{type: "Dominio", level: 90 }]
    },
    {   
        label : "Solución de problemas",
        bar: [{type: "Dominio", level: 95 }]
    },
    {   
        label : "Manejo de personal",
        bar: [{type: "Dominio", level: 95 }]
    },
    {   
        label : "Contacto con proveedores",
        bar: [{type: "Dominio", level: 80 }]
    }, 
    {   
        label : "Trabajo en equipo",
        bar: [{type: "Dominio", level: 95 }]
    },
    {   
        label : "Edición de Videos",
        bar: [{type: "Dominio", level: 80 }]
    },
    {   
        label : "Edición de Imágenes",
        bar: [{type: "Dominio", level: 70 }]
    },
    {   
        label : "Mantenimiento de equipos de cómputo",
        bar:[{type: "Dominio", level: 70 }]
    },
    {   
        label : "Administración de Archivos y recursos",
        bar: [{type: "Dominio", level: 80 }]
    },
    
];
export const software = [
    {   
        label : "Paquetería office",
        bar: [{type: "Dominio", level: 90 }]
    },
    {   
        label : "React JS",
        bar: [{type: "Dominio", level: 50 }]
    },
    {   
        label : "Visual C#",
        bar: [{type: "Dominio", level: 80 }]
    },
    {   
        label : "Oracle Express",
        bar: [{type: "Dominio", level: 85 }]
    },
    {   
        label : "SQL server",
        bar: [{type: "Dominio", level: 80 }]
    },
    
];

export const habilidadesSkillsColor = {
    bar : "#3498db",
    title : {
        text : "#fff",
        background: "#2980b9"
    }
}

export const softwareSkillsColor = {
    bar : "#00bd3f",
    title : {
        text : "#fff",
        background: "#009331"
    }
}