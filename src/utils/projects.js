import cecyte from "../images/projects/cecyte.jpg";
import tec from "../images/projects/tec.png";

export default [
    {
        title : "CECyTE",
        description : "Profesional tecnico en electroica ",
        url: "http://www.cecytebc.edu.mx/aspirantes_planteles_00.asp?p=03&l=00",
        image : cecyte

    },
    {
        title : "Instituto Tecnológico de Tijuana",
        description : "Carrera trunca en ingeniería en sistemas computaciones ",
        url: "https://www.tijuana.tecnm.mx/",
        image : tec

    }
];