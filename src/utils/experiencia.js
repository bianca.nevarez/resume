import lamore from "../images/projects/laMore.jpg";
import eqmedco from "../images/projects/logo-eqmedco.png";
import cmp from "../images/projects/cmp.jpg";
import telvista from "../images/projects/Logo-Telvista.png"
import t33 from "../images/projects/t33.jpg"
export default [
    {
        title : "Panaderia la more",
        description : " Lider de proyectos ",
        url: "https://www.facebook.com/Panaderia.LaMore/",
        image : lamore

    },
    {
        title : "Medical Equipment Distributions - EQmed Co",
        description : " Lead project menager ",
        url: "https://www.facebook.com/eqmedcompany/?ref=page_internal",
        image : eqmedco
    },
    {
        title : "Centro Médico Premier",
        description : " recepcionista médico bilingüe  ",
        url: "https://centromedicopremier.com/",
        image : cmp

    },
    {
        title : "Telvista",
        description : " Agente de soporte técnico  ",
        url: "https://www.telvista.com/es",
        image : telvista

    },
    {
        title : "KCSO Telemundo 33",
        description : " Auxiliar de operaciones ",
        url: "https://www.telemundo33.com/",
        image : t33

    }
];