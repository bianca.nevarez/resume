import React from 'react';
import { Container, Row, Col, Image } from "react-bootstrap";


import profileImage from "../../images/bianca.jpg";
import Social from './Social';
import "./Profile.scss";

const data = [
    {
        title: "Edad:",
        info : "23 años"
    },
    {
        title: "Ciudad:",
        info: "Tijuana B.C."
    },
    {
        title: "E-mail:",
        info: "bianca.nev.lop@gmail.com"
    }//,
   // {
       // title: "telefono",
        //info: "(664) 224-69-68"
    //}
];

export default function Profile() {
    return (
        <div className = "profile">
            <div className = "wallpaper"/>
            <div className = "dark"/>
            <Container className = "box" >
                <Row className = "info">
                    <Col xs = {12} md = {4}>
                        <Image  src={profileImage} fluid />                
                    </Col>
                    <Col xs = {12} md = {8} className = "info__data" >
                        <span>
                            !Hola!
                        </span>
                        <p> Bianca Guadalupe Nevarez Lopez</p>
                        <p>Ingeniero en Sistemas</p>
                        <hr/>
                        <div className="more__info" >
                            {
                                data.map( (item, index) => (
                                    <div key = {index} className = "item">
                                        <p>{item.title}</p>
                                        <p>{item.info}</p>
                                    </div>
                                ) ) 
                            }
                        </div>
                    </Col>
                </Row>
                <Social />
            </Container>
        </div>
    )
}
