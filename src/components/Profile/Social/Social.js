import React from 'react';
import {Facebook, Linkedin, Instagram} from 'react-bootstrap-icons';
import "./Social.scss";


const socialMedia = [
    {
        icon : <Facebook/>,
        link : "https://www.facebook.com/biancaguadalupe.nevarezlopez/" 
    },
    {
        icon : <Linkedin/>,
        link : "https://www.linkedin.com/in/bianca-guadalupe-nevarez-lopez-50a1b8137/" 
    },
    {
        icon : <Instagram/>,
        link : "https://www.instagram.com/biancanev31/" 
    }
];


export default function Social() {
    return (
        <div className = "social">
            
            {
                socialMedia.map( (social, index ) => (
                    <a 
                        key = {index}  
                        href = {social.link} 
                        target = "_blank" 
                        rel= "noreferrer noopener"
                    >
                        {social.icon}
                    </a>
                ))
            }
        </div>
    )
}
