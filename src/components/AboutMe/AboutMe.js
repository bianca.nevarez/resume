import React from 'react';
import { Container, Button } from 'react-bootstrap';

import CV from '../../images/Bianca Nevarez CV.pdf';
import "./AboutMe.scss";

export default function AboutMe() {
    return (
        <Container className = "about-me">
            <p> 
                Tijuanense enamorada de su ciudad y su cultura, amante de las ciencias exactas y la ingeniería. 
                Entusiasta de la tecnología, la fotografía y grado ieby en moo duk kwan. lealtad, honor, respeto,
                perseverancia y honestidad son algunos de los valores que me guían. Interesada por su entorno, 
                empática con sus compañeros, con gran capacidad de liderazgo y habilidad para comunicar sus 
                ideas. He contribuido con mi comunidad, impartiendo asesorías y clases de regularización de diversas 
                materias a niños de escasos recursos y casas hogares.

            </p>
            <hr/>
            <a href={CV} target="_blankc" >
                <Button primary = "true" >Descargar CV</Button>
            </a>
            
        </Container>
    )
}
